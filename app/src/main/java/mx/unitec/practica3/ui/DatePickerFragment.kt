package mx.unitec.practica3.ui

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.format.DateFormat
import android.text.format.DateUtils
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.activity_picker_date.*
import java.text.SimpleDateFormat
import java.util.*

class DatePickerFragment:DialogFragment() {
    private var listener : DatePickerDialog.OnDateSetListener? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val mounth = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        return  DatePickerDialog(activity, listener, year, mounth, day)
        //return DatePickerDialog(activity, listener, year, mounth, day, DateFormat.is24HourFormat(activity))
    }

    companion object{
        fun newInstance(listener: DatePickerDialog.OnDateSetListener):DatePickerFragment{
            val fragment = DatePickerFragment()
            fragment.listener = listener
            return fragment
        }
    }
}
/*
class TimePickerFragment: DialogFragment(){

    private var listener : TimePickerDialog.OnTimeSetListener? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val calendar = Calendar.getInstance()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)


        return TimePickerDialog(activity, listener, hour, minute, DateFormat.is24HourFormat(activity))
    }

    companion object{
        fun newInstance(listener: TimePickerDialog.OnTimeSetListener):TimePickerFragment{
            val fragment = TimePickerFragment()
            fragment.listener = listener
            return fragment
        }
    }
}*/